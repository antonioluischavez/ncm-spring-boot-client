package com.neueda.pocconfigclient;

import com.neueda.ncm.client.spring_boot.config.EnableNeuedaConfigManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableNeuedaConfigManager
public class PocConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocConfigClientApplication.class, args);
	}

}
