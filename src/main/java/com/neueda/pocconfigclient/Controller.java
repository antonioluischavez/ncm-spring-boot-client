package com.neueda.pocconfigclient;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
@RequestMapping("/properties")
public class Controller {

  private final String propertyA;

  private final String propertyB;

  private final Groups groups;

  public Controller(@Value("${propertyA}") String propertyA,
      @Value("${propertyB}") String propertyB, Groups groups) {
    this.propertyA = propertyA;
    this.propertyB = propertyB;
    this.groups = groups;
  }

  @GetMapping("/groups")
  public Map<Integer, List<Integer>> getGroups() {
    return this.groups.getGroups();
  }

  @GetMapping("/a")
  public String getPropertyA() {
    return this.propertyA;
  }

  @GetMapping("/b")
  public String getPropertyB() {
    return this.propertyB;
  }

}
