package com.neueda.pocconfigclient;

import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "dcgw")
public class Groups {

  private static final Logger LOGGER = LoggerFactory.getLogger(Groups.class);

  private Map<Integer, List<Integer>> groups;

  public Map<Integer, List<Integer>> getGroups() {
    return groups;
  }

  public void setGroups(Map<Integer, List<Integer>> groups) {
    this.groups = groups;
  }

  @PostConstruct
  public void onPostConstruct() {
    LOGGER.error("GROUPS BEANS CONSTRUCTED");
  }
}
